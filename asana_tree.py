#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Simple script for reading an asana project tasks and print a dependency tree of
them.

Copyright (C) 2012 Corp B2C S.A.C.

This program is free software; you can redistribute it and/or
modify it under the terms of version 3 of the GNU General Public
License published by the Free Software Foundation.

Needs asana from: https://github.com/pandemicsyn/asana

Author:
    Nicolas Valcárcel Scerpella <nvalcarcel@corpb2c.com>
"""

import re

import asana

API_KEY = ''
PROJECT_ID =  # int

DEP_TAG = ''


class Task(object):
    """
    Task object
    """
    def __init__(self, t_id, api):
        """
        init function
        """
        self._id = t_id

        t_data = api.get_task(self._id)
        self._link = 'https://app.asana.com/0/%d/%d' % (
            t_data['projects'][0]['id'], t_id)
        self._completed = t_data['completed']
        self._name = t_data['name']
        if not t_data['assignee'] is None:
            self._assignee = t_data['assignee']['name']
        else:
            self._assignee = None

        self._stories = api.list_stories(self._id)

        self._get_deps()

    def _get_deps(self):
        """
        Get task deps
        """
        self._deps = []
        pat = re.compile('%s https://app.asana.com/0/\d+/(\d+)' % DEP_TAG)

        for story in self._stories:
            match = pat.match(story['text'])

            if not match is None:
                self._deps.append(int(match.group(1)))

    @property
    def assignee(self):
        return self._assignee

    @property
    def completed(self):
        return self._completed

    @property
    def deps(self):
        return self._deps

    @property
    def link(self):
        return self._link

    @property
    def name(self):
        return self._name


def print_deps(tasks_d, task_id, lvl):
    deps = tasks_d[task_id]['deps']

    for dep in deps:
        task = tasks_d[dep]['task']
        print "%s|-> %s" % ("  " * lvl, task.name)
        if task.deps:
            print_deps(tasks_d, dep, lvl + 1)


def print_tree(tasks_d):
    blocked = []
    deps_l = []
    for key, value in tasks_d.iteritems():
        if value['deps']:
            blocked.append(key)
            deps_l += value['deps']

    tasks_l = list(set(blocked) - set(deps_l))

    for task in tasks_l:
        print "- %s" % tasks_d[task]['task'].name
        print_deps(tasks_d, task, 1)


def main():
    api = asana.AsanaAPI(API_KEY)

    print 'getting project tasks'
    p_tasks = api.get_project_tasks(PROJECT_ID)

    tasks_n = len(p_tasks)
    tasks = {}
    tasks_c = 1
    for task in p_tasks:
        print 'getting task %d/%d' % (tasks_c, tasks_n)
        t_obj = Task(task['id'], api)
        tasks[task['id']] = {'task': t_obj, 'deps': t_obj.deps}
        tasks_c += 1

    print_tree(tasks)


if __name__ == "__main__":
    main()
