#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Simple script for reading an asana project tasks and print tasks without unmet
dependencies.

Copyright (C) 2012 Corp B2C S.A.C.

This program is free software; you can redistribute it and/or
modify it under the terms of version 3 of the GNU General Public
License published by the Free Software Foundation.

Needs asana from: https://github.com/pandemicsyn/asana

Author:
    Nicolas Valcárcel Scerpella <nvalcarcel@corpb2c.com>
"""

import re

import asana

API_KEY = ''
PROJECT_ID =  # integer

DEP_TAG = ''


class Task(object):
    """
    Task object
    """
    def __init__(self, t_id, api):
        """
        init function
        """
        self._id = t_id

        t_data = api.get_task(self._id)
        self._link = 'https://app.asana.com/0/%d/%d' % (
            t_data['projects'][0]['id'], t_id)
        self._completed = t_data['completed']
        self._name = t_data['name']
        if not t_data['assignee'] is None:
            self._assignee = t_data['assignee']['name']
        else:
            self._assignee = None

        self._stories = api.list_stories(self._id)

        self._get_deps()

    def _get_deps(self):
        """
        Get task deps
        """
        self._deps = []
        pat = re.compile('%s https://app.asana.com/0/\d+/(\d+)' % DEP_TAG)

        for story in self._stories:
            match = pat.match(story['text'])

            if not match is None:
                self._deps.append(int(match.group(1)))

    @property
    def assignee(self):
        return self._assignee

    @property
    def completed(self):
        return self._completed

    @property
    def deps(self):
        return self._deps

    @property
    def link(self):
        return self._link

    @property
    def name(self):
        return self._name


def print_ready(tasks_d):
    tasks_l = tasks_d.keys()

    print "Ready to go:"
    for task in tasks_l:
        if not tasks_d[task]['deps'] or tasks_d[task]['task'].completed:
            print "\t- %s" % tasks_d[task]['task'].name
            print "\t  * %s" % tasks_d[task]['task'].link


def main():
    api = asana.AsanaAPI(API_KEY)

    print 'getting project tasks'
    p_tasks = api.get_project_tasks(PROJECT_ID)

    tasks_n = len(p_tasks)
    tasks = {}
    tasks_c = 1
    for task in p_tasks:
        print 'getting task %d/%d' % (tasks_c, tasks_n)
        t_obj = Task(task['id'], api)
        tasks[task['id']] = {'task': t_obj, 'deps': t_obj.deps}
        tasks_c += 1

    print_ready(tasks)
    print_tree(tasks)


if __name__ == "__main__":
    main()
